from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)

posts = [{
    'title': 'Hello world',
    'content': 'This is my first blog post'
}]


@app.route('/')
def home():
    return render_template('home.jinja2', posts=enumerate(posts))


@app.route('/post/<int:post_id>')
def get_post(post_id):
    try:
        post = posts[post_id]
        return render_template('post.jinja2', post=post)
    except IndexError:
        return render_template('404.jinja2', message=f'A post with id {post_id} was not found.')


@app.route('/post/create', methods=['GET', 'POST'])
def post_create():
    if request.method == 'POST':
        title = request.form.get('title')
        content = request.form.get('content')
        id = len(posts)
        posts.append({'id': id, 'title': title, 'content': content})
        return redirect(url_for('get_post', post_id=id))
    return render_template('create.jinja2')



if __name__ == '__main__':
    app.run('localhost', 3000, debug=True)