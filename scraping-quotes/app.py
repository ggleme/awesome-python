import json
from selenium.webdriver import Chrome, ChromeOptions
from pages.quotes_page import QuotesPage
from pages.author_page import AuthorPage
from models.quote import Quote
from models.author import Author
chrome_options = ChromeOptions().add_argument('--headless')
chrome = Chrome(executable_path='/Users/gleme/Downloads/chromedriver', options=chrome_options)
base_url = 'http://quotes.toscrape.com/search.aspx'
chrome.get(base_url)

author = input('Enter the author you\'d like quotes from: ')
page = QuotesPage(chrome)
page.select_author(author)

for quote in page.quotes:
    print(quote)

chrome.close()


# links_authors = [base_url + quote.author_link for quote in quotes]
#
# authors = []
# for link in links_authors:
#     res = requests.get(link)
#     authors.append(AuthorPage(res.content).author)

# quote_objs = []
# for i, quote in enumerate(quotes):
#     author = authors[i]
#     author_obj = Author(name=author.name, birth_date=author.birth_date, born_location=author.born_location, bio=author.bio).__dict__
#     quote_objs.append(Quote(content=quote.content, author=author_obj, tags=quote.tags).__dict__)
#
# with open('export.json', 'w') as file:
#     json.dump(quote_objs, file)
