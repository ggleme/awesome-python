from bs4 import BeautifulSoup
from parsers.author import AuthorParser


class AuthorPage:

    def __init__(self, page):
        self.soup = BeautifulSoup(page, 'html.parser')

    @property
    def author(self):
        return AuthorParser(self.soup)