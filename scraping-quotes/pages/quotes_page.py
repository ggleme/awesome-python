from typing import List
from selenium.webdriver import Chrome
from selenium.webdriver.support.ui import Select

from selectors.quote_page_selectors import QuotesPageSelectors
from parsers.quote import QuoteParser


class QuotesPage:

    def __init__(self, browser: Chrome):
        self.browser = browser

    @property
    def quotes(self) -> List[QuoteParser]:
        quote_tags = self.browser.find_elements_by_css_selector(QuotesPageSelectors.QUOTE)
        return [QuoteParser(e) for e in quote_tags]


    @property
    def author_dropdown(self) -> Select:
        element = self.browser.find_element_by_css_selector(QuotesPageSelectors.AUTHOR_DROPDOWN)
        return Select(element)

    def select_author(self, author_name: str):
        self.author_dropdown.select_by_visible_text(author_name)