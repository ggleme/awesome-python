from selenium import webdriver
from selectors.quote_selectors import QuoteSelectors


class QuoteParser:
    """
    Given one of the specific quote divs, find out the data about
    the quote (quote content, author, tags)
    """
    def __init__(self, parent: webdriver.Chrome):
        self.parent = parent

    def __repr__(self):
        return f'<Quote {self.content}, by {self.author}>'

    @property
    def content(self):
        return self.parent.find_element_by_css_selector(QuoteSelectors.CONTENT).text

    @property
    def author(self):
        return self.parent.find_element_by_css_selector(QuoteSelectors.AUTHOR).text

    @property
    def tags(self):
        return [e.text for e in self.parent.find_element_by_css_selector(QuoteSelectors.TAGS)]

    @property
    def author_link(self):
        return self.parent.find_element_by_css_selector(QuoteSelectors.AUTHOR_LINK).get_attribute('href')
