import aiohttp
import async_timeout
import asyncio
import time
import logging
import requests

from pages.all_books_page import AllBooksPage


logging.basicConfig(
    format='%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
    datefmt='%d-%m-%Y %H%:%M:%S',
    level=logging.DEBUG,
    filename='scraping.log'
)

logger = logging.getLogger('scraping')


async def fetch_page(session, url):
    page_start = time.time()
    async with async_timeout.timeout(10):
        async with session.get(url) as response:
            print(f'The page took {time.time() - page_start} seconds')
            return await response.text()


async def get_multiple_pages(loop, *urls):
    tasks = []
    async with aiohttp.ClientSession(loop=loop) as session:
        for url in urls:
            tasks.append(fetch_page(session, url))
        grouped_tasks = asyncio.gather(*tasks)
        return await grouped_tasks


logger.info('Loading books list...')

response = requests.get('http://books.toscrape.com')
page = AllBooksPage(response.content)
books = page.books

loop = asyncio.get_event_loop()

urls = [f'http://books.toscrape.com/catalogue/page-{page_num + 1}.html' for page_num in range(1, page.page_count)]
start = time.time()
pages = loop.run_until_complete(get_multiple_pages(loop, *urls))

print(f'Total page requests took {time.time() - start} seconds')

for page_num, page in enumerate(pages):
    logger.debug(f'creating page {page_num} content')
    books.extend(AllBooksPage(response.content).books)
