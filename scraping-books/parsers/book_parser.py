import re
import logging
from bs4 import BeautifulSoup

from dataselectors.book_selectors import BookSelectors

logger = logging.getLogger('scraping.book_parser')


class BookParser:

    RATINGS = {
        'One': 1,
        'Two': 2,
        'Three': 3,
        'Four': 4,
        'Five': 5
    }

    def __init__(self, parent_element: BeautifulSoup):
        logger.debug(f'BookParser created from parent element:`{parent_element}`')
        self.parent = parent_element

    def __repr__(self):
        return f'<Book \'{self.name}\', $ {self.price} ({self.rating} star{"s" if self.rating > 1 else ""})>'

    @property
    def name(self):
        logger.debug(f'finding book name...')
        name = self.parent.select_one(BookSelectors.NAME_SELECTOR).attrs.get('title')
        logger.debug(f'found book name: `{name}`')
        return name

    @property
    def link(self):
        logger.debug(f'finding book link...')
        link = self.parent.select_one(BookSelectors.LINK_SELECTOR).attrs.get('href')
        logger.debug(f'found book link: `{link}`')
        return link

    @property
    def price(self):
        logger.debug(f'finding book price...')
        item_price = self.parent.select_one(BookSelectors.PRICE_SELECTOR).string
        matcher = re.search('£([0-9]+\.[0-9]+)', item_price)
        price = float(matcher.group(1))
        logger.debug(f'found book price: `{price}`')
        return price

    @property
    def rating(self):
        logger.debug(f'finding book rating...')
        star_rating_tag = self.parent.select_one(BookSelectors.RATING_SELECTOR)
        classes = star_rating_tag.attrs['class']
        rating_classes = [r for r in classes if r != 'star-rating']
        rating = BookParser.RATINGS.get(rating_classes[0])
        logger.debug(f'found book rating: `{rating}`')
        return rating
