import re
import logging
from bs4 import BeautifulSoup

from dataselectors.all_books_page_selectors import AllBooksPageSelectors
from parsers.book_parser import BookParser

logger = logging.getLogger('scraping.all_books_page')

class AllBooksPage:

    def __init__(self, page_content):
        logger.debug('parsing page content with BeautifulSoup HTML parser.')
        self.soup = BeautifulSoup(page_content, 'html.parser')

    @property
    def books(self):
        logger.debug(f'finding all books in the page using `{AllBooksPageSelectors.BOOKS_SELECTOR}`')
        return [BookParser(el) for el in self.soup.select(AllBooksPageSelectors.BOOKS_SELECTOR)]

    @property
    def page_count(self):
        logger.debug(f'finding all number of catalogue pages using `{AllBooksPageSelectors.BOOKS_SELECTOR}`')
        pager = self.soup.select_one(AllBooksPageSelectors.PAGER_SELECTOR).string
        matcher = re.search('Page [0-9]+ of ([0-9]+)', pager)
        count = int(matcher.group(1))
        logger.debug(f'found {count} pages on the catalogue to extract books info')
        return count
