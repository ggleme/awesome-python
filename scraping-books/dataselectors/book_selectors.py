
class BookSelectors:
    NAME_SELECTOR = 'article.product_pod h3 a'
    LINK_SELECTOR = 'article.product_pod h3 a'
    PRICE_SELECTOR = 'article.product_pod div.product_price p.price_color'
    RATING_SELECTOR = 'article.product_pod p.star-rating'
