import sys
import os
import logging

from app import books

logger = logging.getLogger('scraping.menu')

PROMPT_MENU = '''
Enter one of the following
    - 'b' to look at 5-star books
    - 'c' to look at the cheapest books
    - 'n' to just get the next available book on the catalogue
    - 'q' to exit
Enter an option: '''


def print_best_books():
    logger.info('finding best books...')
    print('\n========== Best Books ==========')
    selected = sorted(books, key=lambda x: (x.rating * -1, x.price))[:10]
    for book in selected:
        print(book)
    print('================================\n')


def print_cheapest_books():
    logger.info('finding best books by price...')
    print('\n======== Cheapest Books ========')
    selected = sorted(books, key=lambda x: (x.price, x.rating * -1))[:10]
    for book in selected:
        print(book)
    print('================================\n')


books_generator = (x for x in books)


def print_next_book():
    logger.info('finding next book...')
    print('\n========== Next Book ===========')
    print(next(books_generator))
    print('================================\n')


operations = {
    'b': print_best_books,
    'c': print_cheapest_books,
    'n': print_next_book,
}


def menu():

    option = input(PROMPT_MENU)
    while option != 'q':
        try:
            operations[option]()
        except KeyError:
            print(f'Invalid option \'{option}\'. Please, try again.')
        finally:
            input('Press enter to continue...')
            sys.stdout.flush()
            os.system('clear')
            option = input(PROMPT_MENU)


menu()
