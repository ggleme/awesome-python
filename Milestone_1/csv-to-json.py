import json

csv_file = open('csv_file.txt', 'r')
csv_data = [line.strip() for line in csv_file.readlines()]
csv_file.close()
teams_data = [data.split(',') for data in csv_data]
teams = [{'club': data[0], 'city': data[1], 'country': data[2]} for data in teams_data]
json_file = open('json-file.txt', 'w')
json.dump(teams, json_file)
json_file.close()
