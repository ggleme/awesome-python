def prompt_movie_info():
    title = input('Enter movie title: ')
    director = input('Enter movie director: ')
    year = input('Enter movie year: ')
    return {
        'title': title,
        'director': director,
        'year': year
    }


def prompt_movie_title():
    return input('Enter movie title: ')


def movie_details(movie):
    return f"""
    Title: {movie['title']}
    Director: {movie['director']}
    Year: {movie['year']}
    """


def add_movie(movies):
    print('\n==== Add Movie ====')
    movie = prompt_movie_info()
    movies.append(movie)


def list_movies(movies):
    print('\n==== Movies List ====')
    for movie in movies:
        print(movie_details(movie))


def find_movie(movies):
    print('\n==== Find Movie ====')
    title = prompt_movie_title()
    for movie in movies:
        if title.lower() == movie['title'].lower():
            print(movie_details(movie))
            break
    else:
        print(f"Movie '{title}' not found.")


MENU_PROMPT = """
==== Moviesy === 
    'a' to add a movie
    'l' to list all movies
    'f' to find a movie by title
    'q' to quit
    Select an option: """

operations = {
    'a': lambda movies: add_movie(movies),
    'l': lambda movies: list_movies(movies),
    'f': lambda movies: find_movie(movies)
}


def main():
    movies = []

    selected_option = input(MENU_PROMPT)

    while selected_option != 'q':
        if selected_option in operations:
            op = operations[selected_option]
            op(movies)
        else:
            print(f"Unkown command '{selected_option}' selected. Please, try again.")

        selected_option = input(MENU_PROMPT)


main()
