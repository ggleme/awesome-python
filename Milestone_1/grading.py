
questions_file = open('questions.txt', 'r')
question_list = [question.strip() for question in questions_file.readlines()]
questions_file.close()

score = 0
for line in question_list:
    q, a = line.split('=')
    answer = input(f"{q}= ")
    if answer == a:
        score += 1

results_file = open('results.txt', 'w')
results_file.write(f'Your final score is {score}/{len(question_list)}.')
results_file.close()
