import re
from bs4 import BeautifulSoup

ITEM_HTML = '''
<html>
    <head></head>
    <body>
        <li class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <article class="product_pod">
                    <div class="image_container">
                            <a href="catalogue/a-light-in-the-attic_1000/index.html">
                                <img src="media/cache/2c/da/2cdad67c44b002e7ead0cc35693c0e8b.jpg" alt="A Light in the Attic" class="thumbnail">
                            </a>
                    </div>
                        <p class="star-rating Three">
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                        </p>
                    <h3>
                        <a href="catalogue/a-light-in-the-attic_1000/index.html" title="A Light in the Attic">
                            A Light in the ...
                        </a>
                    </h3>
                    <div class="product_price">
                        <p class="price_color">£51.77</p>
                        <p class="instock availability">
                            <i class="icon-ok"></i>
                            In stock
                        </p>
                        <form>
                            <button type="submit" class="btn btn-primary btn-block" data-loading-text="Adding...">Add to basket</button>
                        </form>
                    </div>
            </article>
        </li>
    </body>
</html>
'''

rating = {
    'One': 1,
    'Two': 2,
    'Three': 3,
    'Four': 4,
    'Five': 5
}


class ParsedItemSelectors:
    """
    Selectors for an item in the HTML page.

    This allows us to easily see what our code will be looking at
    as well as change it quickly if we notice it is now different.
    """
    NAME_SELECTOR = 'article.product_pod h3 a'
    LINK_SELECTOR = 'article.product_pod h3 a'
    PRICE_SELECTOR = 'article.product_pod p.price_color'
    RATING_SELECTOR = 'article.product_pod p.star-rating'
    

class ParsedItem:
    """
    A class to take in ana HTML page (or part of it), and find properties of an
    item in it.
    """

    def __init__(self, page):
        self.soup = BeautifulSoup(page, 'html.parser')

    @property
    def name(self):
        return self.soup.select_one(ParsedItemSelectors.NAME_SELECTOR).attrs.get('title')

    @property
    def link(self):
        return self.soup.select_one(ParsedItemSelectors.LINK_SELECTOR).attrs.get('href')

    @property
    def price(self):
        item_price = self.soup.select_one(ParsedItemSelectors.PRICE_SELECTOR).string
        matcher = re.search('£([0-9]+\.[0-9]+)', item_price)
        return float(matcher.group(1))

    @property
    def rating(self):
        star_rating_tag = self.soup.select_one(ParsedItemSelectors.RATING_SELECTOR)
        classes = star_rating_tag.attrs['class']
        rating_classes = [r for r in classes if r != 'star-rating']
        return rating[rating_classes[0]]


item = ParsedItem(ITEM_HTML)
print(f"""
    Title: {item.name}
    Price: $ {item.price}
    Rating: {item.rating}
    Link: {item.link}
""")