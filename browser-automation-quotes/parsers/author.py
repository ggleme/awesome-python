from selectors.author_selectors import AuthorSelectors
from bs4 import BeautifulSoup


class AuthorParser:
    """
    Given one of the specific quote divs, find out the data about
    the author (name, birth date, born location, bio)
    """
    def __init__(self, parent: BeautifulSoup):
        self.parent = parent

    def __repr__(self):
        return f'<Author {self.name}>'

    @property
    def name(self):
        return self.parent.select_one(AuthorSelectors.NAME).string

    @property
    def birth_date(self):
        return self.parent.select_one(AuthorSelectors.BIRTH_DATE).string

    @property
    def born_location(self):
        return self.parent.select_one(AuthorSelectors.BORN_LOCATION).string

    @property
    def bio(self):
        return self.parent.select_one(AuthorSelectors.BIO).string
