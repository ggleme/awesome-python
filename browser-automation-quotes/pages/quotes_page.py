from typing import List
from selenium.webdriver import Chrome
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

from selectors.quote_page_selectors import QuotesPageSelectors
from parsers.quote import QuoteParser


class QuotesPage:

    def __init__(self, browser: Chrome):
        self.browser = browser

    @property
    def quotes(self) -> List[QuoteParser]:
        quote_tags = self.browser.find_elements_by_css_selector(QuotesPageSelectors.QUOTE)
        return [QuoteParser(e) for e in quote_tags]


    @property
    def author_dropdown(self) -> Select:
        element = self.browser.find_element_by_css_selector(QuotesPageSelectors.AUTHOR_DROPDOWN)
        return Select(element)

    @property
    def tags_dropdown(self) -> Select:
        element = self.browser.find_element_by_css_selector(QuotesPageSelectors.TAG_DROPDOWN)
        return Select(element)

    @property
    def search_button(self):
        return self.browser.find_element_by_css_selector(QuotesPageSelectors.SEARCH_BUTTON)

    def select_author(self, author_name: str):
        self.author_dropdown.select_by_visible_text(author_name)

    def get_available_tags(self) -> List[str]:
        return [option.text.strip() for option in self.tags_dropdown.options]

    def select_tag(self, tag_name: str):
        self.tags_dropdown.select_by_visible_text(tag_name)

    def search_for_quotes(self, author_name: str, tag_name: str) -> List[QuoteParser]:
        self.select_author(author_name)
        WebDriverWait(self.browser, 10).until(
            expected_conditions.presence_of_element_located(
                (By.CSS_SELECTOR, QuotesPageSelectors.TAG_DROPDOWN_VALUE_OPTION)
            )
        )
        try:
            self.select_tag(tag_name)
        except NoSuchElementException:
            raise InvalidTagForAuthorException(
                f'Author \'{author_name}\' does not have any quote tagged with \'{tag_name}\''
            )
        self.search_button.click()
        return self.quotes


class InvalidTagForAuthorException(ValueError):
    pass
