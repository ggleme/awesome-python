from selenium.webdriver import Chrome, ChromeOptions
from pages.quotes_page import QuotesPage
from pages.quotes_page import InvalidTagForAuthorException
# from pages.author_page import AuthorPage
chrome = None
try:
    author = input('Enter the author you\'d like quotes from: ')
    tag = input('Enter your tag: ')

    chrome_options = ChromeOptions().add_argument('--headless')
    chrome = Chrome(executable_path='/Users/gleme/Downloads/chromedriver', options=chrome_options)
    base_url = 'http://quotes.toscrape.com/search.aspx'
    chrome.get(base_url)
    page = QuotesPage(chrome)

    print(page.search_for_quotes(author, tag))

except InvalidTagForAuthorException as ex:
    print(ex)
except Exception as ex:
    print(ex)
    print('An unkown error occurred.')
finally:
    chrome.close()
# links_authors = [base_url + quote.author_link for quote in quotes]
#
# authors = []
# for link in links_authors:
#     res = requests.get(link)
#     authors.append(AuthorPage(res.content).author)
