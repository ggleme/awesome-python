
class AuthorSelectors:
    NAME = 'h3.author-title'
    BIRTH_DATE = 'span.author-born-date'
    BORN_LOCATION = 'span.author-born-location'
    BIO = 'div.author-description'
